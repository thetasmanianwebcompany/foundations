definitions:
  steps:
    - step: &build
        name: Build
        image: php:8.2.17
        caches:
          - composer
          - node
        script:
          - ####### [Install Composer] #######
          - apt-get update && apt-get install -y unzip
          - export COMPOSER_ALLOW_SUPERUSER=1
          - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
          - ####### [Install NodeJS & npm] #######
          - apt-get install -y nodejs
          - node -v
          - apt-get install -y npm
          - npm -v

          - ####### [Install rsync] #######
          - apt-get install -y rsync
          - rsync -V
          - ####### [Renaming the appropriate composer env file...] #######
          - cp -f composer.json.$BITBUCKET_BRANCH composer.json
          - ####### [Installing WordPress and plugins...] #######
          - composer update --no-dev --no-scripts --optimize-autoloader
          - composer install --no-dev
        artifacts:
          - web/**

    - step: &atomic-rsync
        image: jeremygallant/gitlab-ci-yarn-composer
        caches:
          - composer
          - node
        script:
          - ####### [Install deployment dependencies] #######
          - apt-get install -y rsync
          - apt-get install -y openssh-client
          - apt-get install -y sshpass

          - ####### [Cloudways] #######
          - ####### [Create releases directory] #######
          - if [ $server == 'cloudways' ]; then
          - sshpass -p $server_password ssh -p 22 -o StrictHostKeyChecking=no $server_username@$server_ip << EOF
          - cd public_html/web; if [ ! -d \"releases/$BITBUCKET_COMMIT\" ]; then mkdir -p releases/$BITBUCKET_COMMIT; fi
          - EOF

          - ####### [Rsync files to releases directory] #######
          - sshpass -p $server_password rsync -v -z --delete --recursive --exclude=*.eslintrc.js --exclude=*composer.json.* --exclude=node_modules/ --exclude=*.git --exclude=node_modules --exclude=web/app/mu-plugins/gutenberg-blocks/src/*/*.js --exclude=.cache-loader --exclude=*phpcs*.xml --exclude=*.editorconfig --exclude=web/app/uploads/ --exclude=*.gitignore --exclude=*.scss --exclude=*yarn*.lock -e 'ssh -p 22 -o StrictHostKeyChecking=no' web/ $server_username@$server_ip:public_html/web/releases/$BITBUCKET_COMMIT

          - ####### [Post Deployment Commands] #######
          - sshpass -p $server_password ssh -p 22 -o StrictHostKeyChecking=no $server_username@$server_ip << EOF

          - ####### [Symlink our deployed webroot, but remove the app directory after that] #######
          - find public_html/web -maxdepth 1 -type l -delete

          - ln -sfnr public_html/web/releases/$BITBUCKET_COMMIT/* public_html/web
          - find public_html/web/app -type l -delete

          - ####### [Move into the server dir for easier navigation] #######
          - cd public_html/web

          - ####### [Symlink themes with wp-content] #######
          - find wp-content/themes -maxdepth 1 -type l -delete
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/themes/* wp-content/themes

          - ####### [Symlink mu-plugins with wp-content] #######
          - if [ ! -d \"wp-content/mu-plugins\" ]; then mkdir -p wp-content/mu-plugins; fi
          - find wp-content/mu-plugins -maxdepth 1 -type l -delete
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/mu-plugins/* wp-content/mu-plugins

          - ####### [Symlink plugins with wp-content] #######
          - find wp-content/plugins -maxdepth 1 -type l -delete
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/plugins/* wp-content/plugins

          - ####### [Symlink cache with wp-content] #######
          - rm -r wp-content/cache
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/cache wp-content/cache

          - if [ $BITBUCKET_BRANCH = "staging" ]; then cd releases && ls -t | tail -n +3 | xargs rm -rf; fi

          - if [ $BITBUCKET_BRANCH = "production" ]; then cd releases && ls -t | tail -n +5 | xargs rm -rf; fi

          - wp opcache clear

          - EOF
          - fi

          - ####### [Gridpane] #######
          - ####### [Create releases directory] #######
          - if [ $server == 'gridpane' ]; then
          - ssh -p 22 -o StrictHostKeyChecking=no root@$server_ip << EOF
          - cd www/$server_dir/htdocs; if [ ! -d \"releases/$BITBUCKET_COMMIT\" ]; then mkdir -p releases/$BITBUCKET_COMMIT; fi
          - EOF

          - ####### [Rsync files to releases directory] #######
          - rsync -v -z --delete --recursive --exclude=*.eslintrc.js --exclude=*composer.json.* --exclude=node_modules/ --exclude=*.git --exclude=node_modules --exclude=web/app/mu-plugins/gutenberg-blocks/src/*/*.js --exclude=.cache-loader --exclude=*phpcs*.xml --exclude=*.editorconfig --exclude=web/app/uploads/ --exclude=*.gitignore --exclude=*.scss --exclude=*yarn*.lock -e 'ssh -p 22 -o StrictHostKeyChecking=no' web/ root@$server_ip:www/$server_dir/htdocs/releases/$BITBUCKET_COMMIT

          - ####### [Post Deployment Commands] #######
          - ssh -p 22 -o StrictHostKeyChecking=no root@$server_ip << EOF

          - ####### [Set the correct user permissions on the releases directory] #######
          - chown -R $server_username:$server_username www/$server_dir/htdocs/releases

          - ####### [Symlink our deployed webroot, but remove the app directory after that] #######
          - find www/$server_dir/htdocs -maxdepth 1 -type l -delete

          - ln -sfnr www/$server_dir/htdocs/releases/$BITBUCKET_COMMIT/* www/$server_dir/htdocs
          - find www/$server_dir/htdocs/app -type l -delete

          - ####### [Move into the server dir for easier navigation] #######
          - cd www/$server_dir/htdocs

          - ####### [Symlink themes with wp-content] #######
          - find wp-content/themes -maxdepth 1 -type l -delete
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/themes/* wp-content/themes

          - ####### [Symlink mu-plugins with wp-content] #######
          - if [ ! -d \"wp-content/mu-plugins\" ]; then mkdir -p wp-content/mu-plugins; fi
          - find wp-content/mu-plugins -maxdepth 1 -type l -delete
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/mu-plugins/* wp-content/mu-plugins

          - ####### [Symlink plugins with wp-content] #######
          - find wp-content/plugins -maxdepth 1 -type l -delete
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/plugins/* wp-content/plugins

          - ####### [Symlink cache with wp-content] #######
          - rm -r wp-content/cache
          - ln -sfnr releases/$BITBUCKET_COMMIT/app/cache wp-content/cache

          - if [ $BITBUCKET_BRANCH = "staging" ]; then cd releases && ls -t | tail -n +3 | xargs rm -rf; fi

          - if [ $BITBUCKET_BRANCH = "staging" ]; then gp fix cache staging.$BITBUCKET_REPO_SLUG; fi

          - if [ $BITBUCKET_BRANCH = "production" ]; then cd releases && ls -t | tail -n +5 | xargs rm -rf; fi

          - if [ $BITBUCKET_BRANCH = "production" ]; then gp fix cache $BITBUCKET_REPO_SLUG; fi

          - EOF
          - fi
pipelines:
  branches:
    production:
      - step: *build
      - step:
          <<: *atomic-rsync
          name: Deploy to Production
          deployment: production
    staging:
      - step: *build
      - step:
          <<: *atomic-rsync
          name: Deploy to Staging
          deployment: staging

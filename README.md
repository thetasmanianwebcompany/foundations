# [Foundations](https://bitbucket.org/thetasmanianwebcompany/foundations/)

Foundations is a modern WordPress workflow stack. This stack provides a ready to go development environment, specifically tailored for using GridPane managed VPS development servers and bitbucket deployment, to client server.

* Project specific information should be store/found in the themes `web/app/themes/` directory.

## How it works

Unlike other tools such as Roots Bedrock, Foundations is designed to provide the most native WordPress experience possible while providing repository management of theme and custom plugins, optional and partial composer dependency management and atomic deployments. Foundations comfortably slots in beside any existing WordPress install, creating symlinks between specific directories and the latest deployed build.

## Features

* Consistent development structure
* Optional dependency management with [Composer](https://getcomposer.org)
* Easy WordPress configuration with environment specific files
* Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)
* Autoloader for mu-plugins (use regular plugins as mu-plugins)
* Enhanced security (separated web root and secure passwords with [wp-password-bcrypt](https://github.com/roots/wp-password-bcrypt))
* Composer merging based on env. This allows plugins, themes and other dependencies to be defines by env. Composer merging also provides a simply method to extend and override specific composer scripts.
* The Bitbucket variables and deployments. The build scripts are defined by server provider, and runs the composer scripts and tasks for theme building etc.

## Requirements

* PHP >= 7.1
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* Client server required SSH access with ability to create symlinks, and ideally WP-CLI support.

## Server Support

For development, we recommend using GridPane's managed VPS service. Using VS remote development tools. However there are no restriction on where this can be used.

Production Servers:
* Gridpane
* Cloudways

## Installation

1. Fork this Repository for you project.
3. Add theme(s) in `web/app/themes/` as you would for a normal WordPress site. Any themes requiring build pre-deployment builds should be triggered with a "build" command script in the `web/app/themes/` dir.
4. Add custom mu-plugin(s) in `web/app/mu-plugins/` as you would for a normal WordPress site.
6. Run composer install. This will setup your dev environment.

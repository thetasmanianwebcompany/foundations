<?php
/*
Plugin Name:  ! Define Defaults
Description:  A basic plugin used to define some default across all projects.
Version:      0.3.0
Author:       The Tasmanian Web Company
Author URI:   https://thetasmanianwebcompany.com.au/
License:      MIT License
*/

/**
 * Prevent theme editing and plugin updates by anyone outside our domain
 */
add_action( 'plugins_loaded', function () {
  if (is_user_logged_in() and str_contains(wp_get_current_user()->user_email, 'thetasmanianwebcompany.com.au')) {
    return;
  }

  // Prevent theme editing in wp-admin
  if (! defined('DISALLOW_FILE_EDIT')) {
    define('DISALLOW_FILE_EDIT', true);
  }

  // Disable WordPress core updates
  add_filter('pre_site_transient_update_core', 'disable_updates');

  // Disable WordPress plugin updates
  add_filter('pre_site_transient_update_plugins', 'disable_updates');

  // Disable WordPress theme updates
  add_filter('pre_site_transient_update_themes', 'disable_updates');

});

/**
 * Disable WordPress updates
 * @return object
 */
function disable_updates() {
  global $wp_version;
  return (object) ['last_checked' => time(), 'version_checked' => $wp_version,];
}

/**
 * Disable all auto updates by default, permitting minor core updates and translations
 */
add_filter('automatic_updater_disabled', '__return_true', 1);
add_filter('allow_minor_auto_core_updates', '__return_true', 1);
add_filter('auto_update_translate', '__return_true', 1);

/**
 * Enable Media Trash
 */
if (! defined('MEDIA_TRASH')) {
  define('MEDIA_TRASH', true);
}

/**
 * Post Auto Save Intervals
 */
if (! defined('AUTOSAVE_INTERVAL')) {
  define('AUTOSAVE_INTERVAL', 180);
}

/**
 * Custom MU-Plugin Symlink Location URL Corrector
 * Because we are symlinking our mu-plugins the file dir isn't
 * being stripped because it doesn't align with the content directory.
 * To get around this we can simply string replace our app direct
 */
add_filter('plugins_url', function ($url, $path, $plugin){

  // Check that our plugin in question is a Must Use plugin
  if (false === strpos($url, 'mu-plugins')) {
    return $url;
  }

  // If our mu-plugin is using the correct directory we don't need to do anything.
	if ( ! empty( $plugin ) && false !== strpos( $plugin, WPMU_PLUGIN_URL ) ) {
    return $url;
  } else {
		$url = WPMU_PLUGIN_URL;
  }

  // Remove all content before mu-plugins
  $plugin = strstr($plugin, 'mu-plugins');

  // And now lets remove mu-plugins as well
  $plugin = str_replace('mu-plugins', '', $plugin);

  // Continue processing the url as per WordPress core
  $url = set_url_scheme( $url );

	if ( ! empty( $plugin ) && is_string( $plugin ) ) {
		$folder = dirname( plugin_basename( $plugin ) );
		if ( '.' !== $folder ) {
			$url .= '/' . ltrim( $folder, '/' );
		}
  }

	if ( $path && is_string( $path ) ) {
		$url .= '/' . ltrim( $path, '/' );
	}

  return $url;
}, 11, 3);

/**
 * Extend the life of draft public view posts to 2 weeks
 * @link https://dominikschilling.de/wp-plugins/public-post-preview/en/
 */
add_filter('ppp_nonce_life', function () {
	return 60 * 60 * 24 * 14; // 14 days
});

/**
 * Add emojies to RankMath title to help identify Dev and Staging tabs in browser
 *
 * @link http://www.amp-what.com/unicode/search/book
 *
 * @param string $title
 */
add_filter('rank_math/frontend/title', function($title) {
  if (WP_ENVIRONMENT_TYPE === 'development') {
      return '⛔ ' . $title;
  } elseif (WP_ENVIRONMENT_TYPE === 'staging') {
      return '⚠️ ' . $title;
  }
  return $title;
});
